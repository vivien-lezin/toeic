window.addEventListener('load', function () {
    let formCheckElt = document.querySelectorAll('.form-check');
    let boutonValiderElt = document.querySelector('.btn-primary');
    let boutonSuivantElt = document.querySelector('.btn-success');
    let motElt = document.querySelector('.mot');
    let currentScoreElt = document.querySelector('.curent-score');
    let totalScoreElt = document.querySelector('.total-score');
    let infoElt = document.querySelector('.info');
    let chapitresElt = document.querySelectorAll('.chapitre');
    let boutonCommencerElt = document.querySelector('.btn-info');
    let questionsElt = document.querySelector('.questions');
    let chapitresSectionElt = document.querySelector('.chapitres');
    //Tableaux de mots avec dans chaque cellules contenant le mot en anglais, la traduction, [une info]
    //1. LE PERSONNEL
    let mots1 = [
        ["Human resources", "Les ressources humaines", "Rappelez-vous que ''ressource'' s'écrit avec un seul ''s'' en anglais."],
        ["Job sharing", "Partage des emplois (des fonctions)", ""],
        ["CEO", "Président Directeur Général", "Chief Executive Officier"],
        ["An executive", "Un cadre", ""],
        ["A team leader", "Un chef d'équipe", ""],
        ["A floor manager", "Un chef de rayon", ""],
        ["A boss", "Un patron", ""],
        ["A new hire", "Un nouvel employé", ""],
        ["A temp", "Un travailleur temporaire", "A temporary worker"],
        ["One’s occupation", "Sa profession", "Example: What’s Mike’s occupation ? He’s a detective."],
        ["Affirmative Action", "Un certain organisme pour l'embauche", "Un organisme créé par la loi américaine qui se bat pour l’embauche et la formation des populations minoritaires et des femmes"],
        ["Americans with Disabilities Act", "Une loi américaine qui exige que tout immeuble ou bâtiment soit accessible aux handicapés", ""],
        ["An identification badge", "Un badge d’identité", "An I. D. badge"],
        ["An employee handbook", "Une convention", ""],
        ["Staff attendance", "La présence du personnel", ""],
        ["To be tardy", "Etre en retard", ""],
        ["An employee expense voucher", "Une note de frais", ""],
        ["Upsizing", "Augmentation des effectifs", ""],
        ["Downsizing", "Réduction des effectifs", ""],
        ["A resume", "Un curriculum vitae", ""],
        ["A cover letter", "Une lettre de candidature", ""],
        ["Credentials", "Références", ""],
        ["Skills", "Compétences", ""],
        ["To lay off", "Licencier", ""],
        ["To sack/to fire", "Mettre à la porte", ""],
        ["A pink slip", "Un formulaire rose", "donné par un patron à un employé pour lui communiquer qu’il est mis à la porte."],
        ["Outplacement", "Licenciement accompagné d’aide et de conseils fournis par l’employeur pour trouver un autre emploi", ""],
        ["To resign", "Démissionner", ""],
        ["A resignation", "Une démission", ""],
        ["An employment application form", "Un formulaire de demande d’emploi auprès d’un employeur", ""],
        ["A want ad", "Une offre d’emploi", ""],
        ["To hire", "Embaucher", ""],
        ["To be on strike", "Être en grève", ""],
        ["Training", "Une formation", ""]
    ];
    //2. LA FABRICATION
    let mots2 = [
        ["Plant/factory", "Une usine", ""],
        ["To work in shifts", "Travailler les trois huitièmes", "Example: Dave works the day shift. Day shift, afternoon shift, night shift"],
        ["A level", "Un niveau", ""],
        ["An assembly line", "Une chaîne de montage", ""],
        ["To be suitable", "Être convenable", ""],
        ["To suit", "Convenir", ""],
        ["To hamper", "Entraver", ""],
        ["Outsourcing", "La sous-traitance", ""],
        ["Parts", "Des pièces détachées", ""],
        ["To shut down", "Fermer", ""],
        ["To revamp", "Restructurer", ""],
        ["A supervisor/foreman", "Un contre-maître", ""],
        ["To replace", "Remplacer", ""],
        ["Industry", "Secteur", "Example: the automotive industry, the pharmaceutical industry"],
        ["Reliable", "Fiable", ""],
        ["Unreliable", "Peu fiable", ""],
        ["A helmet", "Un casque", ""]
    ];
    //3. LES LOCAUX
    let mots3 = [
        ["A branch", "Une agence", ""],
        ["Subsidiary", "Une filiale", ""],
        ["A merger", "Une fusion", ""],
        ["Headquarters", "Un siège social", ""],
        ["A warehouse", "Un entrepôt", ""],
        ["Real estate", "L’immobilier", ""],
        ["A groundbreaking (ceremony)", "Une cérémonie de début de chantier", ""],
        ["A utility", "Un service public", "Example: Northeast Utilities sent Sue her water bill."],
        ["A takeover", "Une OPA", ""],
        ["To move (The premises)", "Déménager", ""],
        ["A facility", "Un aménagement", ""],
        ["A hall/corridor", "Un couloir", ""],
        ["An acquisition", "Un achat", ""],
        ["To acquire", "Acheter", ""],
        ["To own", "Posséder", ""],
        ["An elevator", "Un ascenseur", ""],
        ["The premises", "Le lieu de travail, les locaux", ""]
    ];
    //4. LE BUREAU
    let mots4 = [
        ["A memo/memorandum", "Une note interne", ""],
        ["Office chores", "Les tâches au bureau", ""],
        ["Office supplies", "Fournitures de bureau", ""],
        ["Stationery", "Papier lettre", ""],
        ["The letterhead", "L’en-tête", ""],
        ["A file", "Un dossier", ""],
        ["To file", "Classer les dossiers", ""],
        ["A thumb tack", "Une punaise", ""],
        ["A stapler", "Une agrafeuse", ""],
        ["A copier", "Une photocopieuse", ""],
        ["To sort the mail", "Trier le courrier", ""],
        ["Correction fluid", "Tip-Ex", ""],
        ["A pencil", "Un crayon", ""],
        ["A Pen", "Un Stylo", ""],
        ["A highlighter", "Un feutre", ""],
        ["An eraser", "Une gomme", ""],
        ["A paper clip", "Un trombone", ""],
        ["A bulletin board", "Un tableau d’affichage", ""],
        ["A lunch break", "Une pause déjeuner", ""],
        ["A coffee break", "Une pause café", ""],
        ["Petty cash", "La caisse", ""],
        ["A chart", "Un tableau", ""],
        ["A graph", "Un graphique", ""],
        ["To be available", "Être disponible", ""],
        ["To enclose", "Joindre", ""],
        ["To tidy", "Ranger", ""],
        ["Clutter", "Pagaille, entassement", ""],
        ["A shelf", "Un rayon, une étagère", ""],
        ["A toll-free call", "Un appel en numéro vert", ""],
        ["A dial tone", "Une tonalité (téléphone)", ""],
        ["A task", "Une tâche", ""],
        ["A schedule", "Un emploi du temps", ""],
        ["Business hours", "Les heures du bureau", ""],
        ["A receptionist", "Une standardiste", ""],
        ["A storage room", "Un lieu de stockage, un dépôt", ""]
    ];
    //5. LE VOCABULAIRE TECHNIQUE
    let mots5 = [
        ["A cellular telephone", "Un téléphone portable", ""],
        ["A printer", "Une imprimante", ""],
        ["A panel", "Un panneau", ""],
        ["A floppy disk", "Une disquette", ""],
        ["A disk drive", "Un lecteur de disquettes", ""],
        ["To plug in", "Brancher", ""],
        ["A toner cartridge", "Une cartouche de toner", ""],
        ["A monitor/screen", "Un écran", ""],
        ["Software", "Un logiciel", ""],
        ["Hardware", "Le disque dur", ""],
        ["To upgrade", "Ajouter de la mémoire", ""],
        ["A keyboard", "Un clavier", ""],
        ["A network", "Un réseau", ""],
        ["A quick start guide", "Un manuel d’utilisation rapide", ""],
        ["To log on", "Entrer/se connecter", ""],
        ["To log off", "Sortir/se déconnecter ", ""]
    ];
    //6. LA SANTÉ
    let mots6 = [
        ["Ill/sick", "Malade", ""],
        ["A physician", "Un médecin", ""],
        ["Health benefits", "Les avantages de l’assurance maladie", ""],
        ["Major medical", "Le nom de l’assurance maladie américaine payée par l’employeur", ""],
        ["Dental coverage", "La couverture dentaire", ""],
        ["A sick day", "Un jour de congé maladie", ""],
        ["Lifestyle", "Mode de vie", ""],
        ["A fitness center", "Une salle de sport", ""]
    ];
    //7. LES FINANCES ET LES BUDGETS
    let mots7 = [
        ["An account", "Un compte", ""],
        ["Accounting", "La comptabilité", ""],
        ["A quarter", "Un trimestre", ""],
        ["An electronic funds transfer / An ATM", "Un virement", "Automatic Teller Machine : un distributeur de billets"],
        ["Growth", "Croissance", ""],
        ["Decline", "Déclin", ""],
        ["To rise/increase", "Augmenter", ""],
        ["To slump/fall/decrease", "Baisser, chuter", ""],
        ["A bonus", "Une prime", ""],
        ["A figure/turnover", "Un chiffre/chiffre d’affaires", ""],
        ["A rate", "Un taux", ""],
        ["To forecast/predict", "Prévoir", ""],
        ["Stockholders/stocks", "Les actionnaires/actions", ""],
        ["To cut funding", "Supprimer des fonds", ""],
        ["To raise money", "Récolter des fonds", ""],
        ["An auditor", "Un commissaire aux comptes, un vérificateur des comptes", ""],
        ["A loss", "Une perte", ""],
        ["To go bankrupt/out of business", "Faire faillite", ""],
        ["A loan", "Un prêt", ""],
        ["A Mortgage", "Une hypothèque", ""],
        ["Payday", "Le jour de paie", ""],
        ["Payroll", "Le grand livre de paie", ""],
        ["Pay slip", "La fiche de paie", ""],
        ["Expenses", "Des dépenses", ""],
        ["Costs", "Des coûts", ""],
        ["A fee", "Un tarif", ""],
        ["A price list", "Un tarif (liste)", "Example: The doctor’s fee was $20. We can’t charge you until we check the price list."],
        ["The annual report", "Le rapport annuel", ""],
        ["The financial report", "Le rapport financier", ""],
        ["To break even", "Atteindre le seuil de rentabilité", ""]
    ];
    //8. LA RECHERCHE ET DÉVELOPPEMENT
    let mots8 = [
        ["A brand", "Une marque", ""],
        ["User-friendly", "Convivial, facile à utiliser", ""],
        ["To be eligible", "Avoir droit", "Example: She wasn’t eligible for a tax break this year."],
        ["A goal", "Un objectif", ""],
        ["To switch", "Changer", "Example: The laboratory switched formulas."],
        ["To meet a deadline", "Respecter un délai", ""],
        ["A tight deadline", "Un délai, une date de limite serrée", ""],
        ["A target", "Une cible", ""],
        ["A target audience", "Un public ciblé", ""],
        ["A tip (developement)", "Un conseil, un tuyau", "Example: His coworker gave him a tip about that chemical formula."],
        ["A specific request", "Une demande précise", ""]
    ];
    //9. LE VOCABULAIRE DES AFFAIRES
    let mots9 = [
        ["A bill/invoice", "Une facture", ""],
        ["A customer", "Un client", ""],
        ["A profit", "Un bénéfice", ""],
        ["A department", "Un service", "Example : the marketing department, the sales department"],
        ["Small business", "Les petites et les moyennes entreprises", ""],
        ["To run a business", "Diriger une société", ""],
        ["Market share", "La part de marché", ""],
        ["Fulltime", "À plein temps", "Example: on a parttime basis"],
        ["Parttime", "À mi-temps", "Example: on a parttime basis"],
        ["Board members", "Membres du Conseil d’Administration", ""],
        ["Minutes of a meeting", "Compte-rendu d’une réunion", ""],
        ["An appointment", "Un rendez-vous", ""],
        ["To hold a conference", "Tenir un congrès, une conférence", ""],
        ["To attend a workshop", "Assister à un atelier", ""],
        ["A delay", "Un retard", ""],
        ["Attire", "Les vêtements, uniformes", "Example: Only formal attire is accepted at that restaurant."],
        ["An asset", "Les avoirs, les actifs", ""],
        ["A fire drill", "Un exercice de sauvetage en cas d’incendie", ""],
        ["To check", "Vérifier", ""],
        ["Overtime", "Des heures supplémentaires", ""],
        ["A poll/survey", "Un sondage", ""],
        ["A general ledger", "Le grand livre de frais", ""],
        ["The management", "La direction", ""],
        ["Telecommuting", "Travailler de chez soi (souvent grâce à l’Internet)", ""],
        ["To convene", "Convoquer, réunir", ""],
        ["To cancel", "Annuler", ""],
        ["To schedule an appointment", "Fixer un rendez-vous", ""],
        ["To require", "Exiger", ""],
        ["Workload", "Une quantité de travail", ""],
        ["Backlog", "Du retard dans son travail", ""],
        ["Wages", "Solde, salaire", ""]
    ];
    //10. LES ACHATS ET LES VENTES
    let mots10 = [
        ["A sales policy", "Une politique de vente", ""],
        ["An estimate/quotation", "Un devis", ""],
        ["Delivery", "Livraison", ""],
        ["Shipping", "Expédition", ""],
        ["A sample", "Un échantillon", ""],
        ["A warranty/guarantee", "Une garantie", ""],
        ["A mall/shopping center", "Un centre commercial", ""],
        ["A sale", "Les soldes", "Example: They’re having a sale at the mall. All bikes are 30 % off today."],
        ["An item", "Un article", ""],
        ["Extra", "Supplémentaire", ""],
        ["An order form", "Un bon de commande", ""],
        ["To place an order", "Passer une commande", ""],
        ["A receipt", "Un reçu", ""],
        ["A retailer", "Un détaillant", ""],
        ["A wholesaler", "Un grossiste", ""],
        ["To pay in full", "Régler toute la facture", ""],
        ["Consumer behavior", "Le comportement du consommateur", ""],
        ["An aisle", "Une allée", ""],
        ["A display", "Un présentoir", ""],
        ["A refund", "Un remboursement", ""],
        ["A commitment", "Un engagement", ""],
        ["A dealer", "Un concessionnaire", ""]
    ];
    //11. LES RESTAURANTS
    let mots11 = [
        ["A menu", "Une carte", ""],
        ["All you can eat", "À volonté", ""],
        ["An appetizer", "Une entrée", ""],
        ["An entree", "Un plat", ""],
        ["A cocktail", "Un apéritif", ""],
        ["A waiter", "Un serveur", ""],
        ["A waitress", "Une serveuse", ""],
        ["A balanced meal", "Un repas équilibré", ""],
        ["The check", "L’addition", ""],
        ["A tip (restaurant)", "Un pourboire", ""],
        ["A chef", "Un chef de cuisine", ""],
        ["A roll", "Un petit pain", ""],
        ["Soup of the day", "La soupe du jour", ""],
        ["Caterer", "Un traiteur", ""],
        ["Fast-food", "La restauration rapide", ""],
        ["Tap water", "L’eau du robinet", ""],
        ["Sparkling water", "L’eau pétillante", ""],
        ["Cashier", "La caisse", ""],
        ["Silverware", "Les couverts", ""],
        ["Tablecloth", "Une nappe", ""],
        ["Napkin", "Une serviette", ""],
        ["Dish", "Une assiette", ""],
        ["Cup", "Une tasse", ""],
        ["Glass", "Un verre", ""],
        ["To order", "Commander", ""]
    ];
    //12. LES VOYAGES
    let mots12 = [
        ["Luggage", "Les bagages", ""],
        ["A direct flight", "Un vol direct", ""],
        ["A connecting flight", "Un vol avec correspondance", ""],
        ["A ski resort", "Une station de ski", ""],
        ["A resort", "Un village de vacances", ""],
        ["A round trip flight", "Un vol aller-retour", ""],
        ["A one-way flight", "Un vol simple", ""],
        ["A rental car", "Une voiture de location", ""],
        ["High season", "La haute saison", ""],
        ["Off season", "La basse saison", ""],
        ["A cruise", "Une croisière", ""],
        ["A subway", "Le métro", ""],
        ["A fare", "Un tarif (d’avion, etc.)", ""],
        ["A boarding pass", "Une carte d’embarquement", ""],
        ["To check in", "Se présenter/s’enregistrer", ""],
        ["A train conductor", "Un contrôleur de train", ""],
        ["A flight attendant", "Une hôtesse de l’air", ""],
        ["Train tracks", "Les rails", ""],
        ["Train crossing", "Un passage à niveau", ""],
        ["A night clerk", "Un veilleur de nuit", ""],
        ["Traveller’s checks", "Les chèques de voyage", ""],
        ["The room rate", "Le prix d’une chambre", ""]
    ];
    //13. LE SPECTACLE
    let mots13 = [
        ["A performance", "Une représentation", ""],
        ["A play", "Une pièce de théâtre", ""],
        ["A show", "Un spectacle", ""],
        ["Tickets", "Les places", ""],
        ["Intermission", "L’entracte", ""],
        ["A movie", "Un film", ""],
        ["The opening (of a film)", "La sortie d’un film", ""],
        ["A blockbuster", "Un film grand public", ""],
        ["A review", "Une critique", ""],
        ["A director", "Un metteur en scène", ""],
        ["A producer", "Un producteur", ""],
        ["A script", "Un scénario", ""],
        ["A lead", "Un premier rôle", ""],
        ["A supporting role", "Un deuxième rôle", ""],
        ["An Oscar/an Academy Award", "Un césar", ""],
        ["An amusement park", "Un parc d’attraction", ""],
        ["A ride", "Un manège", ""],
        ["A fair", "Une foire", ""],
        ["An exhibit", "Une exposition", ""],
        ["A lecture", "Une conférence", ""],
        ["A star", "Une vedette", ""],
        ["A novel", "Un roman", ""],
        ["A novelist", "Un romancier", ""],
        ["An award", "Un prix", ""]
    ];
    //16. LISTE DES «PHRASAL VERBS»
    let mots16 = [
        ["Back out", "Changer d’avis", ""],
        ["Back up", "Reculer", ""],
        ["Be bound to", "Être certain de", ""],
        ["Be had", "Être roulé, trompé", ""],
        ["Be into", "Se passionner pour", ""],
        ["Be on hand", "Être à portée de la main", ""],
        ["Be on time", "Être à l’heure", ""],
        ["Be out of date", "Être démodé ; périmé", ""],
        ["Be over", "Être fini", ""],
        ["Be up", "Être terminé", ""],
        ["Be up to date", "Être à jour", ""],
        ["Bow out", "Ne plus participer", ""],
        ["Break down", "Tomber en panne", ""],
        ["Break loose", "Se détacher de", ""],
        ["Break off", "Couper", ""],
        ["Break out", "Éclater", ""],
        ["Bring about", "Provoquer", ""],
        ["Bring back", "Rapporter", ""],
        ["Bring up", "Élever", ""],
        ["Bring out", "Faire paraître", ""],
        ["Build up", "Renforcer", ""],
        ["Buy out", "Acheter la part de", ""],
        ["Buy up", "Faire l’achat total", ""],
        ["Call for", "Venir chercher", ""],
        ["Call off", "Annuler", ""],
        ["Call on", "Visiter", ""],
        ["Carry on", "Continuer", ""],
        ["Carry out", "Exécuter", ""],
        ["Catch on", "Comprendre", ""],
        ["Catch up", "Rattraper", ""],
        ["Check on/check up on", "Vérifier", ""],
        ["Clear up", "Clarifier", ""],
        ["Come about", "Se produire", ""],
        ["Come across", "Trouver par hasard", ""],
        ["Come to", "Se remettre", ""],
        ["Come true", "Se réaliser", ""],
        ["Count on", "Compter sur", ""],
        ["Cover for", "Remplacer", ""],
        ["Cover up", "Dissimuler", ""],
        ["Cut in", "Interrompre", ""],
        ["Cut off", "Couper; interrompre", ""],
        ["Cut out", "Cesser de", ""],
        ["Cut short", "Couper court", ""],
        ["Cut up", "Couper en morceaux", ""],
        ["Die down", "S’éteindre", ""],
        ["Die out", "Disparaître", ""],
        ["Draw up", "Préparer", ""],
        ["Drop in", "Visiter à l’imprévu", ""],
        ["Drop off", "Déposer", ""],
        ["Drop out", "Quitter", ""],
        ["Dry out", "Sécher", ""],
        ["Dry up", "Dessécher", ""],
        ["Fade away", "Décroître", ""],
        ["Fall behind", "Être en arrière", ""],
        ["Fall off", "Tomber de", ""],
        ["Fall through", "Échouer", ""],
        ["Figure out", "Calculer", ""],
        ["Fill out, fill in", "Remplir", ""],
        ["Get along", "S’entendre ; progresser", ""],
        ["Get along with", "S’entendre", ""],
        ["Get away", "S’échapper", ""],
        ["Get away with", "S’en tirer", ""],
        ["Get back", "Retourner", ""],
        ["Get carried away", "Se laisser emporter", ""],
        ["Get lost", "Se perdre", ""],
        ["Get over", "Se remettre", ""],
        ["Get rid of", "Se débarrasser de", ""],
        ["Get through", "Terminer", ""],
        ["Get to", "Arriver à", ""],
        ["Give in", "Céder", ""],
        ["Give off", "Produire", ""],
        ["Give out", "Distribuer", ""],
        ["Give up", "Abandonner", ""],
        ["Go around", "Suffire à tout le monde", ""],
        ["Go in for", "S’adonner à", ""],
        ["Go on", "Continuer", ""],
        ["Go out", "Sortir", ""],
        ["Go through", "Subir", ""],
        ["Go wrong", "Marcher mal", ""],
        ["Grow out of", "Passer", ""],
        ["Hand in", "Remettre", ""],
        ["Hear from", "Recevoir des nouvelles", ""],
        ["Hear of", "Entendre parler de", ""],
        ["Hold off", "Retenir", ""],
        ["Hold out", "S’arrêter", ""],
        ["Hold over", "Continuer à montrer", ""],
        ["Hold up", "Arrêter la circulation", ""],
        ["Keep on", "Continuer à", ""],
        ["Keep out", "Défendre d’entrer", ""],
        ["Keep track of", "Enregistrer", ""],
        ["Keep up", "Maintenir", ""],
        ["Keep up with", "Aller aussi vite que", ""],
        ["Knock it off", "Cesser immédiatement", ""],
        ["Lay off", "Mettre au chômage", ""],
        ["Let go of", "Lâcher", ""],
        ["Let up", "Diminuer", ""],
        ["Live it up", "Mener la belle vie", ""],
        ["Look after", "S’occuper de", ""],
        ["Look down on", "Regarder de haut en bas", ""],
        ["Look into", "Examiner avec soin", ""],
        ["Look out", "Faire attention", ""],
        ["Look out on", "Donner sur", ""],
        ["Look over", "Examiner", ""],
        ["Look up", "Chercher", ""],
        ["Look up to", "Avoir un grand respect pour", ""],
        ["Make do", "Se débrouiller", ""],
        ["Make good", "Réussir", ""],
        ["Make out", "Déchiffrer", ""],
        ["Make sure", "S’assurer de", ""],
        ["Make up", "Se rattraper ; inventer", ""],
        ["Pass out", "Distribuer", ""],
        ["Pick out", "Choisir", ""],
        ["Pick up", "Ramasser", ""],
        ["Play up to", "Flatter", ""],
        ["Point out", "Signaler", ""],
        ["Pull off", "Réussir", ""],
        ["Put away", "Ranger", ""],
        ["Put down", "Déposer", ""],
        ["Put off", "Remettre", ""],
        ["Put out", "Éteindre", ""],
        ["Put together", "Assembler", ""],
        ["Put up", "Construire", ""],
        ["Put up with", "Tolérer", ""],
        ["Rule out", "Éliminer", ""],
        ["Run into", "Rencontrer par hasard", ""],
        ["Run over", "Écraser", ""],
        ["See about", "S’occuper de", ""],
        ["Sell out", "Liquider", ""],
        ["Set out", "Se mettre en chemin", ""],
        ["Show off", "Se vanter", ""],
        ["Show up", "Se présenter", ""],
        ["Shut off", "Fermer", ""],
        ["Shut up", "Se taire", ""],
        ["Stand for", "Représenter", ""],
        ["Stand out", "Se distinguer de", ""],
        ["Stand up", "Résister ; poser un lapin", ""],
        ["Step down", "Démissionner", ""],
        ["Stick around", "Rester", ""],
        ["Stick to", "Persévérer", ""],
        ["Stir up", "Agiter", ""],
        ["Take after", "Tenir de", ""],
        ["Take care of", "Prendre soin de", ""],
        ["Take down", "Prendre note de", ""],
        ["Take hold of", "Saisir", ""],
        ["Take in", "Voir", ""],
        ["Take off", "Enlever ; décoller", ""],
        ["Take on", "Engager", ""],
        ["Take out", "Sortir", ""],
        ["Take over", "Se charger de", ""],
        ["Take part in", "Participer à", ""],
        ["Take place", "Avoir lieu", ""],
        ["Take up", "Étudier", ""],
        ["Talk back", "Rétorquer", ""],
        ["Talk over", "Discuter", ""],
        ["Tear down", "Démolir", ""],
        ["Tear up", "Déchirer", ""],
        ["Think of", "Penser à", ""],
        ["Think over", "Réfléchir", ""],
        ["Think up", "Imaginer", ""],
        ["To be better off", "Valoir mieux", ""],
        ["To be well off", "Vivre dans l’aisance", ""],
        ["Top something", "Dépasser", ""],
        ["Try on ; try out", "Essayer", ""],
        ["Turn down", "Refuser", ""],
        ["Turn off", "Éteindre", ""],
        ["Turn on", "Allumer", ""],
        ["Turn out", "Devenir", ""],
        ["Wait for", "Attendre", ""],
        ["Wait on", "Servir", ""],
        ["Wear down", "User", ""],
        ["Wear off", "Disparaître", ""],
        ["Wear out", "User", ""],
        ["Work out", "Bien finir", ""]
    ];
    //Tableaux regroupant tout les tableaux demandés
    let mots = [];

    function random(nbr) {
        return Math.trunc(Math.random() * nbr, 0);
    }

    //Génération des listeners pour les check box chapitres
    for (let i = 0; i < chapitresElt.length; i++) {
        chapitresElt[i].addEventListener('click', function (e) {
            e.preventDefault;

            e.target.classList.toggle('checked');
            let x = false;
            for (let j = 0; j < chapitresElt.length; j++) {
                if (chapitresElt[j].classList.contains('checked')) {
                    x = true;
                }
            }
            if (!x) {
                boutonCommencerElt.classList.add('disabled');
                boutonCommencerElt.removeEventListener('click', commencer);
            } else {
                boutonCommencerElt.addEventListener('click', commencer);
                boutonCommencerElt.classList.remove('disabled');
            }
        });
    }

    function commencer() {
        for (let j = 0; j < chapitresElt.length; j++) {
            if (chapitresElt[j].classList.contains('checked')) {
                if (chapitresElt[j].id === "chapitre1") {
                    mots = mots.concat(mots1);
                }
                if (chapitresElt[j].id === "chapitre2") {
                    mots = mots.concat(mots2);
                }
                if (chapitresElt[j].id === "chapitre3") {
                    mots = mots.concat(mots3);
                }
                if (chapitresElt[j].id === "chapitre4") {
                    mots = mots.concat(mots4);
                }
                if (chapitresElt[j].id === "chapitre5") {
                    mots = mots.concat(mots5);
                }
                if (chapitresElt[j].id === "chapitre6") {
                    mots = mots.concat(mots6);
                }
                if (chapitresElt[j].id === "chapitre7") {
                    mots = mots.concat(mots7);
                }
                if (chapitresElt[j].id === "chapitre8") {
                    mots = mots.concat(mots8);
                }
                if (chapitresElt[j].id === "chapitre9") {
                    mots = mots.concat(mots9);
                }
                if (chapitresElt[j].id === "chapitre10") {
                    mots = mots.concat(mots10);
                }
                if (chapitresElt[j].id === "chapitre11") {
                    mots = mots.concat(mots11);
                }
                if (chapitresElt[j].id === "chapitre12") {
                    mots = mots.concat(mots12);
                }
                if (chapitresElt[j].id === "chapitre13") {
                    mots = mots.concat(mots13);
                }
                if (chapitresElt[j].id === "chapitre16") {
                    mots = mots.concat(mots16);
                }
            }
        }
        
        genererMots();
        boutonValiderElt.classList.remove('d-none');
        questionsElt.classList.remove('d-none');
        chapitresSectionElt.classList.add('d-none');
        console.log(mots);
    }


    function genererMots() {
        //Index du mot à trouver
        let indexMot;

        //O anglais 1 traductions
        let langue;

        //Correspond à la traduction à trouver
        let invLangue;

        // Mot courant à chercher
        let mot;


        //tableau de vérification de la non-duplication des mots
        let valeurs = [];

        function genererIndexAleatoire() {

            let x = true;
            let valeur;
            while (x) {
                x = false;
                valeur = random(mots.length - 1);
                for (let j = 0; j < valeurs.length; j++) {
                    if (valeur === valeurs[j]) {
                        x = true;
                    }
                }
            }

            return valeur;

        }



        for (let i = 0; i < 6; i++) {
            valeurs.push(genererIndexAleatoire());
        }

        indexMot = random(6);

        langue = random(2);

        if (langue) {
            invLangue = 0;
        } else {
            invLangue = 1;
        }

        console.log(valeurs);

        motElt.textContent = mots[valeurs[indexMot]][langue];
        motElt.id = valeurs[indexMot];
        console.log(motElt);

        for (let i = 0; i < formCheckElt.length; i++) {
            if (i === indexMot) {
                formCheckElt[i].textContent = mots[valeurs[indexMot]][invLangue];
                formCheckElt[i].id = valeurs[indexMot];
            } else {
                formCheckElt[i].textContent = mots[valeurs[i]][invLangue];
                formCheckElt[i].id = valeurs[i];
            }

        }
        for (let i = 0; i < formCheckElt.length; i++) {
            formCheckElt[i].addEventListener('click', checker);
            formCheckElt[i].classList.remove('bg-danger');
            formCheckElt[i].classList.remove('bg-success');
        }
        boutonValiderElt.classList.remove('d-none');
        boutonValiderElt.classList.add('disabled');
        boutonSuivantElt.classList.add('d-none');

        infoElt.textContent = "";
    }


    boutonSuivantElt.addEventListener('click', genererMots);

    function verificationResultat() {
        boutonValiderElt.removeEventListener('click', verificationResultat);
        boutonValiderElt.classList.add('d-none');
        boutonSuivantElt.classList.remove('d-none');
        let reussi = false;
        for (let j = 0; j < formCheckElt.length; j++) {
            if (formCheckElt[j].classList.contains('checked')) {
                if (motElt.id === formCheckElt[j].id) {
                    formCheckElt[j].classList.add('bg-success');
                    reussi = true;
                } else {
                    formCheckElt[j].classList.add('bg-danger');
                }
                formCheckElt[j].classList.remove('checked');
            }

            if (motElt.id === formCheckElt[j].id) {
                formCheckElt[j].classList.add('bg-success');
            }
            formCheckElt[j].removeEventListener('click', checker);
        }
        totalScoreElt.textContent = parseInt(totalScoreElt.textContent) + 1;
        if (reussi) {
            currentScoreElt.textContent = parseInt(currentScoreElt.textContent) + 1;
        }
        if (mots[motElt.id][2] !== "") {
            infoElt.innerHTML = "<b>Info - </b>" + mots[motElt.id][2];
        }

    }

    function checker(e) {
        boutonValiderElt.removeEventListener('click', verificationResultat);
        e.preventDefault;
        for (let j = 0; j < formCheckElt.length; j++) {
            formCheckElt[j].classList.remove('checked');
        }

        e.target.classList.toggle('checked');

        boutonValiderElt.classList.remove('disabled');
        boutonValiderElt.addEventListener('click', verificationResultat);
    }

});
